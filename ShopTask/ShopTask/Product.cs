﻿using System;

namespace ShopTask
{
    class Product
    {
        public string Description { get; }
        public string Name { get; }
        public decimal price { get; }

        public Product(string description, string name, decimal price)
        {
            Description = description;
            Name = name;
            this.price = price;
        }

        public override bool Equals(object obj)
        {
            var product = obj as Product;
            return product != null &&
                   Description == product.Description &&
                   Name == product.Name &&
                   price == product.price;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Description, Name, price);
        }

        public override string ToString()
        {
            return Description+" "+ Name+" "+price;
        }

    }
}
