﻿using System;
using System.Collections.Generic;

namespace ShopTask
{
    class Customer
    {
        public String name { get; }
        private int cardNumber;
        static double cardRandom = 1000.0;
        public List<Order> orders = new List<Order>();
        public Decimal innerBalance { get; set; }  //inner currency balance



        public Decimal GetCardBalance()
        {
            // here will be transaqtion
            Random random = new Random(cardNumber);
            return (decimal)(random.NextDouble() * cardRandom);
        }

        public Customer(string name, int cardNumer, Decimal innerBalance = 0)
        {
            this.name = name;
            this.cardNumber = cardNumer;
            this.innerBalance = innerBalance;
        }

        public void Pay(Decimal price)
        {
                // payment method with credit card
        }
        public Order OrderFromCode(int code)
        {
            IEnumerator<Order> iter = orders.GetEnumerator();
            while (iter.MoveNext())
            {
                if (iter.Current.GetHashCode() == code)
                    return iter.Current;
            }
            return null;
        }
    }
}
