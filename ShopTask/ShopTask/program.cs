﻿using System;
using System.Collections.Generic;

namespace ShopTask
{
    class program
    {
        static public void Main()
        {
            Shop shop = new Shop();
            shop.products.Add(new Product("Соленость", "Огурцы Бабушкина Кухня", 10M));
            shop.products.Add(new Product("Колбасы", "Докторская", 20M));
            shop.products.Add(new Product("Сыры", "Российский", 8M));
            shop.products.Add(new Product("Хлебобулочные", "Хлеб", 3M));
            shop.products.Add(new Product("Кондитерские изделия", "Торт", 22M));

            Customer customer = new Customer("Ivan", 1234);

            shop.ShowProducts();
            Console.WriteLine(customer.innerBalance);
            shop.Buy(customer, shop.Order(customer, "Хлеб", "Хлеб", "Торт", "Клей"));

            customer.innerBalance = 100;
            Console.WriteLine(customer.innerBalance);
            shop.Buy(customer, shop.Order(customer, "Хлеб", "Хлеб", "Торт", "Клей"));

            Console.ReadLine();
        }
    }
}
