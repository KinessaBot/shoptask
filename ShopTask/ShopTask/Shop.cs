﻿using System;
using System.Collections.Generic;

namespace ShopTask
{
    class Shop
    {
        public List<Product> products = new List<Product>();

        public int Order(Customer customer, params Product[] product)
        {
            Order order = new Order(DateTime.UtcNow, customer, product);
            customer.orders.Add(order);
            return order.GetHashCode();
        }

        public int Order(Customer customer, params int[] productcodes)
        {
            Product[] products = new Product[productcodes.Length];
            int j = 0, i = 0;
            for (; j < productcodes.Length; j++)
            {
                Product product = ProductFromCode(productcodes[j]);
                if (!(product is null))
                {
                    products[i] = product;
                    i++;
                }
                else
                {
                    Console.WriteLine("Can't find product with code: " + productcodes[j]);
                }
            }
            Array.Resize<Product>(ref products, products.Length - j + i);
            Order order = new Order(DateTime.UtcNow, customer, products);
            customer.orders.Add(order);
            return order.GetHashCode();
        }

        public int Order(Customer customer, params string[] productnames)
        {
            Product[] products = new Product[productnames.Length];
            int j = 0, i = 0;
            for ( ; j < productnames.Length; j++)
            {
                Product product = ProductFromName(productnames[j]);
                if (!(product is null))
                {
                    products[i] = product;
                    i++;
                }
                else
                {
                    Console.WriteLine("Can't find product with name: " + productnames[j]);
                }
            }
            Array.Resize<Product>(ref products, products.Length-j+i);
            Order order = new Order(DateTime.UtcNow, customer, products);
            customer.orders.Add(order);
            return order.GetHashCode();
        }

        public void ShowProducts()
        {
            IEnumerator<Product> iter = products.GetEnumerator();
            while (iter.MoveNext())
            {
                Console.WriteLine(iter.Current.ToString());
            }
        }

        public void Buy(Customer customer, Order order, bool innerBalance = true)
        {
            Decimal price = order.GetTotalPrice();
            if (innerBalance)
            {
                if (customer.innerBalance >= price)
                {
                    customer.innerBalance -= price;
                    Console.WriteLine("Customer " + customer.name + " successfully bought with inner currency on " + price + ". " + DateTime.UtcNow);
                    // sending products
                    return;
                }
            }
            else
            {
                if (price <= customer.GetCardBalance())
                {
                    customer.Pay(price);
                    Console.WriteLine("Customer " + customer.name + " successfully bought with credit card on " + price + ". " + DateTime.UtcNow);
                    // sending products
                    return;
                }               
            }
            Console.WriteLine("Not enougth money to pay");
        }

        public void Buy(Customer customer, int orderscode, Customer customer2 = null, bool innerBalance = true)
        {
            Customer cc = customer;
            if(!(customer2 is null))
            {
                cc = customer2;
            }
            Order order = cc.OrderFromCode(orderscode);
            if (order is null)
            {
                Console.WriteLine("Can't fint order with code: " + orderscode);
                return;
            }
            this.Buy(customer, order);
        }
        private int CodeFromName(string name)
        {
            IEnumerator<Product> iter = products.GetEnumerator();
            while (iter.MoveNext())
            {
                if (String.Compare(name, iter.Current.Name) == 0)
                    return iter.Current.GetHashCode();
            }
            return -1;
        }

        private Product ProductFromCode(int code)
        {
            IEnumerator<Product> iter = products.GetEnumerator();
            while (iter.MoveNext())
            {
                if (iter.Current.GetHashCode()==code)
                    return iter.Current;
            }
            return null;
        }

        private Product ProductFromName(string name)
        {
            IEnumerator<Product> iter = products.GetEnumerator();
            while (iter.MoveNext())
            {
                if (iter.Current.Name == name)
                    return iter.Current;
            }
            return null;
            //return ProductFromCode(CodeFromName(name));
        }
    }
}
