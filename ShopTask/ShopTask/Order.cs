﻿using System;
using System.Collections.Generic;

namespace ShopTask
{
    class Order
    {
        public Product[] products { get; }
        public DateTime date { get; }
        public Customer customer { get; }

        public Order(DateTime date, Customer customer, Product[] products)
        {
            this.products = products;
            this.date = date;
            this.customer = customer;
        }

        public decimal GetTotalPrice()
        {
            decimal total =0M;
            for (int i = 0;i< products.Length;i++)
            {
                if(!(products[i] is null))
                    total += products[i].price;
            }
            return total;
        }

        public override bool Equals(object obj)
        {
            var order = obj as Order;
            return order != null &&
                   EqualityComparer<Product[]>.Default.Equals(products, order.products) &&
                   date == order.date &&
                   EqualityComparer<Customer>.Default.Equals(customer, order.customer);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(products, date, customer);
        }

    }
}
